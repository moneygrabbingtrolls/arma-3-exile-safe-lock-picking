#define _ARMA_
class CfgPatches
{
	class alp
	{
		units[] = {};
		weapons[] = {};
		UD_Version = 0.1;
		requiredVersion = 1.36;
		requiredAddons[] = {};
	};
};
class CfgFunctions
{
	class alp
	{
		class main
		{
			file = "a3_exile_autolockpicker";
			class alpinit
			{
				postInit = 1;
			};
		};
	};
};