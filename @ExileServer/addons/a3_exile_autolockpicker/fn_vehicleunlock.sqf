if(isServer) then 
{
	"ALPUNLOCK" addPublicVariableEventHandler {_this select 1 call unlock_vehicle};	
	unlock_vehicle = {	private["_veh"];
		_veh = _this select 0;
		_veh setVariable ["ExileIsLocked", 0, true];
		_veh setVariable ["ExileAccessCode","0000"];
		_location = _this select 1;
		_player = _this select 2;
		_playerUID = _this select 3;
		["toastRequest", ["InfoTitleAndText", ["A prisoner cracked a safe @", _location]]] call ExileServer_system_network_send_broadcast;
		_safehack = format ["PLAYER: ( %1 ) %2 CRACKED A SAFE @ %3",_player,_playerUID,_location];
		//comment out below if not using infiSTAR
		["SAFEHACK",_safehack] call FNC_A3_CUSTOMLOG;
	};
};